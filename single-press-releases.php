<?php

get_header();

$show_default_title = get_post_meta( get_the_ID(), '_et_pb_show_title', true );

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );
?>

<div id="main-content">
	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">
				<?php
				while ( have_posts() ) :
					the_post();
					?>
					<?php
					/**
					 * Fires before the title and post meta on single posts.
					 *
					 * @since 3.18.8
					 */
					do_action( 'et_before_post' );
					?>
					<article id="post-<?php the_ID(); ?>" <?php post_class( 'et_pb_post' ); ?>>
						<?php
							do_action( 'et_before_content' );
						?>
						<?php echo do_shortcode( '[load_divi_layout id="5337"]' ); ?>
						<div id="wpr_main_content_div">
							<div class="et_pb_section et_pb_with_background et_section_regular" id="breadcrumbs_section">
								<?php echo do_shortcode( '[load_divi_layout id="5349"]' ); ?>
							</div>
							<div class="entry-content <?php echo $class_add; ?>">
								<div class="content_div">
									<?php
										do_action( 'et_before_content' );
									?>
									<h1 class="single_title"><?php the_title(); ?></h1>
									<div class="single_meta"><?php echo get_the_date( 'F d, Y' ); ?></div>
									<div class="tags_list">
										<?php echo get_the_term_list( get_the_ID(), 'press-releases-tag-cat', '', '', '' ); ?>
									</div>
									<div class="content_text_div">
										<div class="content_text_div_text">
											<?php
												the_content();
											?>
										</div>
										<div class="share_media_div">
											<span><?php echo __( 'Share this story:', 'wpr' ); ?></span>
											<?php
											$page_url = get_permalink();
											?>
											<ul class="share_media_ul">
												<li>
													<a class="social-links__link" href="https://twitter.com/intent/tweet?text=<?php echo $page_url; ?>" rel="noopener" target="_blank">
														<span id="hs_cos_wrapper_footer_social_" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_icon social-links__icon" style="" data-hs-cos-general-type="widget" data-hs-cos-type="icon">
															<svg version="1.0" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" aria-labelledby="share"><title id="twitter-share">Share on Twitter</title><g id="layer1"><path d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"></path></g></svg>
														</span>
													</a>  
												</li>
												<li>
													<a class="social-links__link" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $page_url; ?>" rel="noopener" target="_blank">
														<span id="hs_cos_wrapper_footer_social__2" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_icon social-links__icon" style="" data-hs-cos-general-type="widget" data-hs-cos-type="icon"><svg version="1.0" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 264 512" aria-labelledby="facebook-f-share"><title id="facebook-f-share">Share on Facebook</title><g id="layer1"><path d="M76.7 512V283H0v-91h76.7v-71.7C76.7 42.4 124.3 0 193.8 0c33.3 0 61.9 2.5 70.2 3.6V85h-48.2c-37.8 0-45.1 18-45.1 44.3V192H256l-11.7 91h-73.6v229"></path></g></svg></span>
													</a>
												</li>
												<li>
													<a class="social-links__link" href="https://www.linkedin.com/shareArticle?mini=true&title=&summary=&source=&url=<?php echo $page_url; ?>" rel="noopener" target="_blank">
														<span id="hs_cos_wrapper_footer_social__3" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_icon social-links__icon" style="" data-hs-cos-general-type="widget" data-hs-cos-type="icon"><svg version="1.0" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" aria-labelledby="linkedin-in-share"><title id="linkedin-in-share">Share on LinkedIn</title><g id="layer1"><path d="M100.3 480H7.4V180.9h92.9V480zM53.8 140.1C24.1 140.1 0 115.5 0 85.8 0 56.1 24.1 32 53.8 32c29.7 0 53.8 24.1 53.8 53.8 0 29.7-24.1 54.3-53.8 54.3zM448 480h-92.7V334.4c0-34.7-.7-79.2-48.3-79.2-48.3 0-55.7 37.7-55.7 76.7V480h-92.8V180.9h89.1v40.8h1.3c12.4-23.5 42.7-48.3 87.9-48.3 94 0 111.3 61.9 111.3 142.3V480z"></path></g></svg></span>
													</a>
												</li>
												<li>
													<a class="social-links__link" href="mailto:info@example.com?&subject=&body=<?php echo $page_url; ?>" rel="noopener" target="_blank">
														<span id="hs_cos_wrapper_footer_social__3" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_icon social-links__icon" style="" data-hs-cos-general-type="widget" data-hs-cos-type="icon"><svg version="1.0" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" aria-labelledby="email-share"><title id="email-share">Share on Email</title><g id="layer1"><path d="M464 64H48C21.49 64 0 85.49 0 112v288c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V112c0-26.51-21.49-48-48-48zm0 48v40.805c-22.422 18.259-58.168 46.651-134.587 106.49-16.841 13.247-50.201 45.072-73.413 44.701-23.208.375-56.579-31.459-73.413-44.701C106.18 199.465 70.425 171.067 48 152.805V112h416zM48 400V214.398c22.914 18.251 55.409 43.862 104.938 82.646 21.857 17.205 60.134 55.186 103.062 54.955 42.717.231 80.509-37.199 103.053-54.947 49.528-38.783 82.032-64.401 104.947-82.653V400H48z" class=""></path></g></svg></span>
													</a>
												</li>
												<li>
													<a class="social-links__link" href="https://www.twitte22222r.com" rel="noopener" target="_blank">
														<span id="hs_cos_wrapper_footer_social__3" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_icon social-links__icon" style="" data-hs-cos-general-type="widget" data-hs-cos-type="icon"><svg version="1.0" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" aria-labelledby="twitter-share"><title id="twitter-share">Share on Twitter</title><g id="layer1"><path d="M100.3 480H7.4V180.9h92.9V480zM53.8 140.1C24.1 140.1 0 115.5 0 85.8 0 56.1 24.1 32 53.8 32c29.7 0 53.8 24.1 53.8 53.8 0 29.7-24.1 54.3-53.8 54.3zM448 480h-92.7V334.4c0-34.7-.7-79.2-48.3-79.2-48.3 0-55.7 37.7-55.7 76.7V480h-92.8V180.9h89.1v40.8h1.3c12.4-23.5 42.7-48.3 87.9-48.3 94 0 111.3 61.9 111.3 142.3V480z"></path></g></svg></span>
													</a>
												</li>
												<li>
													<a class="social-links__link" href="http://pinterest.com/pin/create/button/?url=<?php echo $page_url; ?>" rel="noopener" target="_blank">
														<span id="hs_cos_wrapper_footer_social__3" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_icon social-links__icon" style="" data-hs-cos-general-type="widget" data-hs-cos-type="icon"><svg version="1.0" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><title id="pinterest-share">Share on Pinterest</title><g id="layer1"><path d="M204 6.5C101.4 6.5 0 74.9 0 185.6 0 256 39.6 296 63.6 296c9.9 0 15.6-27.6 15.6-35.4 0-9.3-23.7-29.1-23.7-67.8 0-80.4 61.2-137.4 140.4-137.4 68.1 0 118.5 38.7 118.5 109.8 0 53.1-21.3 152.7-90.3 152.7-24.9 0-46.2-18-46.2-43.8 0-37.8 26.4-74.4 26.4-113.4 0-66.2-93.9-54.2-93.9 25.8 0 16.8 2.1 35.4 9.6 50.7-13.8 59.4-42 147.9-42 209.1 0 18.9 2.7 37.5 4.5 56.4 3.4 3.8 1.7 3.4 6.9 1.5 50.4-69 48.6-82.5 71.4-172.8 12.3 23.4 44.1 36 69.3 36 106.2 0 153.9-103.5 153.9-196.8C384 71.3 298.2 6.5 204 6.5z" class=""></path></g></svg></span>
													</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div> <!-- .entry-content -->
						<div class="section" id="under_text_elements">
							<?php echo do_shortcode( '[load_divi_layout id="5419"]' ); ?>
							<?php echo do_shortcode( '[load_divi_layout id="4555"]' ); ?>
							<?php echo do_shortcode( '[load_divi_layout id="3023"]' ); ?>
						</div>
						<div class="et_post_meta_wrapper">
							<?php

							/**
							 * Fires after the post content on single posts.
							 *
							 * @since 3.18.8
							 */
							do_action( 'et_after_post' );
							?>
						</div> <!-- .et_post_meta_wrapper -->
					</article> <!-- .et_pb_post -->

				<?php endwhile; ?>
			</div> <!-- #left-area -->
		</div> <!-- #content-area -->
	</div> <!-- .container -->
</div> <!-- #main-content -->

<?php

get_footer();
