<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Child Styles
function wpr_child_styles() {
	wp_enqueue_style( 'wpr-child-style', trailingslashit( get_template_directory_uri() ) . 'style.css' );
}

add_action( 'wp_enqueue_scripts', 'wpr_child_styles' );


// Changing Read More text
function wpr_divi_readmore_text( $translated_text, $text, $domain ) {
	switch ( $translated_text ) {
		case 'read more':
			$translated_text = __( 'Full Article', 'et_builder' );
			break;
	}
	return $translated_text;
}
add_filter( 'gettext', 'wpr_divi_readmore_text', 20, 3 );


// Add Social Media To Mobile Menu
add_filter( 'wp_nav_menu_items', 'wpr_add_social_to_respponsive_menu', 10, 2 );
function wpr_add_social_to_respponsive_menu( $olditems, $args ) {
	if ( $args->theme_location == 'primary-menu' ) {

				$logo = get_stylesheet_directory_uri() . '/img/visage-light.svg';

		$items         .= '<li><a class="mobile-nav-logo" href="' . esc_url( home_url( '/' ) ) . '">';
				$items .= '	<img src="' . esc_attr( $logo ) . '" />';
				$items .= '</a></li>';
		$items         .= $olditems;
		$items         .= '<div class="wpr-nav-bottom D">';
		$items         .= '		<ul class="wpr-nav-social">';
		$items         .= '				<li><a class="twitter" href="https://twitter.com/Visage_Imaging"></a></li>';
		$items         .= '				<li><a class="facebook" href="https://www.facebook.com/VisageImaging/"></a></li>';
		$items         .= '				<li><a class="linkedin" href="https://www.linkedin.com/company/visage-imaging/"></a></li>';
				$items .= '		</ul>';
				$items .= '		<p>' . __( '©️ 2020 Visage Imaging, Inc.', 'divi' ) . '</p>';
				$items .= '</div>';
		return $items;
	} else {
		return $olditems;
	}

}

require_once 'wpr_includes/functions_tim.php';


// Register Custom Post type for Vacancies

// Register Custom Post Type
function wpr_vacancy_custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Vacancies', 'runa' ),
		'singular_name'         => _x( 'Vacancy', 'runa' ),
		'menu_name'             => __( 'Vacancies', 'runa' ),
		'name_admin_bar'        => __( 'Vacancy', 'runa' ),
		'all_items'             => __( 'All Vacancies', 'runa' ),
		'add_new_item'          => __( 'Add New Vacancy', 'runa' ),
		'add_new'               => __( 'Add New Vacancy', 'runa' ),
		'new_item'              => __( 'New Vacancy', 'runa' ),
		'edit_item'             => __( 'Edit Vacancy', 'runa' ),
		'update_item'           => __( 'Update Vacancy', 'runa' ),
		'view_item'             => __( 'View Vacancy', 'runa' ),
		'view_items'            => __( 'View Vacancies', 'runa' ),
		'search_items'          => __( 'Search Vacancy', 'runa' ),
	);
	$args = array(
		'label'                 => __( 'Vacancies', 'runa' ),
		'labels'                => $labels,
		'supports'              => array("title","editor","thumbnail"),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 25,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'Vacancies', $args );

}
add_action( 'init', 'wpr_vacancy_custom_post_type', 0 );




// Showing Vacancies Post Types

function wpr_vacancies_functions(){

	$args = array(
			'post_type' => 'vacancies',
	);

	$query = new WP_Query( $args );
 
	if ( $query->have_posts() ) {

			$content .="<div class='type-vacancies wpr-toggle'>";
				$content .="<div class='et_pb_accordion et_pb_accordion_0'>";
					
					while ( $query->have_posts() ) : $query->the_post();

							$content .= "<div class='et_pb_toggle et_pb_module et_pb_accordion_item wpr_job_". sanitize_title_with_dashes( esc_attr( get_the_title() ) ) ." et_pb_accordion_item_0 et_pb_toggle_close'>";
							$content .= '		<div class="wpr-vacancy-header">';

							if( has_post_thumbnail() ){ 
							$content .= '			<div class="wpr-vacancy-thumbnail">';
							$content .= 				get_the_post_thumbnail();
							$content .= '			</div>';		
							}
							
							$content .= '			<div class="et_pb_toggle_title">';
							
							if( get_field("country_flag_1") || get_field("country_flag_2") || get_field("country_flag_3") || get_field("country_flag_4") ){
									
									$content .= '				<div class="wpr-vacancy-flags">';
									
									if( get_field("country_flag_1") ){
										$content .= '			<img src="'. esc_url( get_field("country_flag_1")['url'] ) .'">';  
									}
									
									if( get_field("country_flag_2") ){
										$content .= '			<img src="'. esc_url( get_field("country_flag_2")['url'] ) .'">';  
									}
									
									if( get_field("country_flag_3") ){
										$content .= '			<img src="'. esc_url( get_field("country_flag_3")['url'] ) .'">';  
									}

									
									if( get_field("country_flag_4") ){
										$content .= '			<img src="'. esc_url( get_field("country_flag_4")['url'] ) .'">';  
									}


									$content .= '				</div>';

							}

							$content .= '				<h4>'. esc_attr( get_the_title() )  .'</h4>';

							if( get_field("vacancy_subtitle") ){
							$content .= '				<p class="wpr-subtitle">'. esc_attr( get_field("vacancy_subtitle") )  .'</p>';
							}

							$content .= '			</div>';
							$content .= '		</div>';
							$content .= '		<div class="et_pb_toggle_content clearfix">';
							$content .= '			<div class="wpr-column">';
							$content .= 					get_the_content();
							$content .=	'				<div class="et_pb_button_module_wrapper wpr-apply-button et_pb_button_0_wrapper et_pb_button_alignment_phone_center et_pb_module">';
							$content .=	'						<a class="et_pb_button et_pb_button_0 et_pb_bg_layout_light" href="mailto:jobs@visageimaging.com">Apply Now </a>';
							$content .=	'				</div>';
							$content .=	'			</div>';
							$content .= '		</div>';
							$content .= '</div>';

					endwhile;

			} else {
						$content .= "No Vacancy Found";
			}
			
		$content .="</div>";
	$content .="</div>";


wp_reset_postdata();
 
return $content;

}

add_shortcode('wpr_vacancies', 'wpr_vacancies_functions');


// Adding Fall-back image to Home Hero Video

function wpr_detect_ios_version(){
    if(stripos($_SERVER['HTTP_USER_AGENT'],"iPhone") || stripos($_SERVER['HTTP_USER_AGENT'],"iPad")) {
        return preg_replace("/(.*) OS ([0-9]*)_(.*)/","$2", $_SERVER['HTTP_USER_AGENT']);
    } else {
        return false;
    }
}


if(wpr_detect_ios_version() && wpr_detect_ios_version() < 10 ){
    

		function wpr_add_inline_css() {

				wp_enqueue_style(
					'wpr-custom-style',
					get_stylesheet_directory_uri() . '/css/wpr-custom-style.css'
				);

				$custom_css = ".et_pb_section_video_bg{ display:none; }";

				wp_add_inline_style( 'wpr-custom-style', $custom_css );
		}
		add_action( 'wp_enqueue_scripts', 'wpr_add_inline_css' );


  }


