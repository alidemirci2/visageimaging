<?php
add_action( 'init', 'wpr_load_shortcodes' );

function wpr_load_shortcodes() {
	add_shortcode( 'wpr_events_list', 'wpr_events_list_funct' );
	add_shortcode( 'wpr_event_home', 'wpr_event_home_funct' );
	add_shortcode( 'wpr_blog_home', 'wpr_blog_home_funct' );
	add_shortcode( 'load_divi_layout', 'load_divi_layout_func' );
	add_shortcode( 'wpr_add_navigation', 'wpr_add_navigation_func' );
	add_shortcode( 'wpr_before_search_content', 'wpr_before_search_content_func' );
	add_shortcode( 'wpr_search_content', 'wpr_search_content_func' );
}

function wpr_before_search_content_func() {
	return '<h1 class="single_title">' . __( 'Search results for:', 'wpr' ) . ' ' . get_search_query() . '</h1>';
}
function wpr_add_navigation_func() {
	$html = '';
	ob_start();
	get_template_part( 'includes/navigation', 'index' );
	$html = ob_get_contents();
	ob_end_clean();
	return $html;
}

function wpr_search_content_func() {
	$html = '';
	ob_start();
	?>
	<div id="taxonomy_content_div" class="wpr_realeses_listing">
		<?php if ( have_posts() ) : ?>
			<?php
			while ( have_posts() ) :
				the_post();
				?>
				<article id="post-<?php echo get_the_ID(); ?>" class="press-releases type-press-releases">
					<h2 class="entry-title"><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></h2>
					<p class="post-meta">
						<span class="published"><?php echo get_the_date( 'M d, Y' ); ?></span>
					</p>
					<p></p>
					<div class="post-content">
						<div class="post-content-inner">
							<?php the_excerpt(); ?>
						</div>
						<a href="<?php echo get_permalink(); ?>" class="more-link"><?php echo __( 'Full Article', 'wpr' ); ?></a>
					</div>
				</article>
			<?php endwhile; ?>
			<?php
			get_template_part( 'includes/navigation', 'index' );
		else :
			echo esc_html__( 'No results found', 'wpr' );
		endif;
		?>
	</div> <!-- .entry-content -->
	<?php
	$html = ob_get_contents();
	ob_end_clean();
	return $html;
}

function wpr_events_list_funct() {
	$html       = '';
	$date       = (int) date( 'Ymd' );
	$args       = array(
		'post_type'      => 'event',
		'posts_status'   => array( 'publish' ),
		'posts_per_page' => 99,
		'meta_key'       => 'event_start_date',
		'orderby'        => 'meta_value_num',
		'order'          => 'ASC',
		'meta_query'     => array(
			'relation' => 'OR',
			array(
				'relation' => 'AND',
				array(
					'key'     => 'event_start_date',
					'compare' => '<=',
					'value'   => $date,
					'type'    => 'numeric',
				),
				array(
					'key'     => 'event_end_date',
					'compare' => '>=',
					'value'   => $date,
					'type'    => 'numeric',
				),
			),
			array(
				'relation' => 'AND',
				array(
					'key'     => 'event_start_date',
					'compare' => '>=',
					'value'   => $date,
					'type'    => 'numeric',
				),
				array(
					'key'     => 'event_end_date',
					'compare' => '>=',
					'value'   => $date,
					'type'    => 'numeric',
				),
			),
		),
	);
	$posts_list = query_posts( $args );

	if ( have_posts() ) {
		$html .= '<div class="events_listing">';
		while ( have_posts() ) {
			the_post();

			$period_date = wpr_show_period( get_the_id() );
			if ( ! empty( $period_date ) ) {
				$period_date = '<div class="period">' . $period_date . '</div>';
			}
			$location = get_field( 'event_location', get_the_id() );
			if ( ! empty( $location ) ) {
				$location = '<div class="location">' . $location . '</div>';
			}

			$html         .= '<div class="event_single">';
				$html     .= '<div class="col_1 col_location hide_mobile">' . $period_date . $location . '</div>';
				$html     .= '<div class="col_2">';
					$html .= '<h2><a href="' . get_permalink() . '">' . get_the_title() . '</a></h2>';
					$html .= '<div class="col_location hide_desktop show_mobile">' . $period_date . $location . '</div>';
					$html .= '<div class="excerpt_event">' . get_the_excerpt() . '</div>';
				$html     .= '</div>';
				$html     .= '<div class="col_3"><a href="' . get_permalink() . '" class="view_details">' . __( 'View Details', 'wpr' ) . '</a></div>';
			$html         .= '</div>';
		}
		$html .= '</div>';
	}
	wp_reset_query();

	return $html;
}

function wpr_event_home_funct() {
	$html     = '';
	$event_id = get_option( 'event_settings_homepage' );
	if ( ! empty( $event_id ) ) {
		$event_data = get_post( $event_id );
		$content    = get_field( 'event_home_content', $event_id );
		$permalink  = get_permalink( $event_data );
		$att_id     = get_post_thumbnail_id( $event_data );
		$att_src    = get_permalink( $att_id );
		$att_srcset = wp_get_attachment_image_srcset( $att_id, 'large' );

		$html = '<div class="wpr-home-blog event_home">
		    <div class="et_pb_ajax_pagination_container">
                <article id="post-' . $event_data->ID . '" class="et_pb_post clearfix et_pb_blog_item_1_0 post-' . $event_data->ID . ' event type-event status-publish has-post-thumbnail hentry">
                    <a href="' . $permalink . '" class="entry-featured-image-url">
                        <img src="' . $att_src . '" alt="' . $event_data->post_title . '" class="" width="100%" height="auto" srcset="' . $att_srcset . '">
                    </a>
                    <h2 class="entry-title">
                        <a href="' . $permalink . '">' . $event_data->post_title . '</a>
                    </h2>
                    <p class="post-meta"></p>
                    <div class="post-content">
                        <div class="post-content-inner et_pb_blog_show_content">' . $content . '</div>
                    </div>			
                </article>
            </div>
        </div>';
	} else {
		$html .= '<div class="wpr-home-blog event_home">' . __( 'No future Events found', 'wpr' ) . '</div>';
	}

	return $html;
}

function wpr_blog_home_funct() {
	global $wpr_post_transient_name;
	$html = '';
	$tmp  = get_transient( $wpr_post_transient_name );

	$data_article = json_decode( $tmp );
	unset( $tmp );

	$html = '<div class="wpr-home-blog event_home">
		<div class="et_pb_ajax_pagination_container">
			<article id="post-latest" class="et_pb_post clearfix et_pb_blog_item_1_0 post-latest event type-event status-publish has-post-thumbnail hentry">
				<a href="' . $data_article->link . '" class="entry-featured-image-url">
					<img src="' . $data_article->image . '" alt="' . $data_article->title . '" title="' . $data_article->title . '" class="" width="100%" height="auto" />
				</a>
				<h2 class="entry-title">
					<a href="' . $data_article->link . '">' . $data_article->title . '</a>
				</h2>
				<p class="post-meta"></p>
				<div class="post-content">
					<div class="post-content-inner et_pb_blog_show_content">' . strip_tags( $data_article->body ) . '[...]</div>
				</div>
				<a href="' . $data_article->link . '" class="read_full">' . __( 'Full Article', 'wpr' ) . '</a>
			</article>
		</div>
	</div>';

	return $html;
}

function load_divi_layout_func( $atts, $content = '' ) {
	extract(
		shortcode_atts(
			array(
				'id'          => '',
				'add_section' => '',
				'add_row'     => '',
			),
			$atts
		)
	);

	if ( ! empty( $atts['id'] ) ) {
		$html  = '';
		$html .= ( 'true' === $add_section ? '<div class="et_pb_section et_pb_with_background et_section_regular">' : '' );
		$html .= ( 'true' === $add_row ? '<div class="et_pb_row">' : '' );
		$html .= do_shortcode( '[et_pb_section global_module="' . $atts['id'] . '"][/et_pb_section]' );
		$html .= ( 'true' === $add_row ? '</div>' : '' );
		$html .= ( 'true' === $add_section ? '</div>' : '' );

		return $html;
	}
}
