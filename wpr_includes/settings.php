<?php
// create Events settings page
add_action( 'admin_menu', 'events_settings_create_menu' );

function events_settings_create_menu() {
	add_submenu_page( 'edit.php?post_type=event', 'Events Settings', 'Events Settings', 'administrator', 'events_settings', 'events_settings_page', 'dashicons-admin-tools' );
	add_action( 'admin_init', 'register_events_settings' );
}


function register_events_settings() {
	register_setting( 'events_settings_group', 'event_settings_homepage' );
	register_setting( 'events_settings_group', 'event_settings_default_header' );
	register_setting( 'events_settings_group', 'event_settings_default_image_1' );
	register_setting( 'events_settings_group', 'event_settings_default_image_2' );
	register_setting( 'events_settings_group', 'event_settings_redirect' );
}

function events_settings_page() {
	$date             = (int) date( 'Ymd' );
	$args             = array(
		'post_type'      => 'event',
		'posts_per_page' => -1,
		'fields'         => '',
		'meta_key'       => 'event_start_date',
		'orderby'        => 'meta_value_num',
		'order'          => 'ASC',
		'meta_query'     => array(
			'relation' => 'OR',
			array(
				'relation' => 'AND',
				array(
					'key'     => 'event_start_date',
					'compare' => '<=',
					'value'   => $date,
					'type'    => 'numeric',
				),
				array(
					'key'     => 'event_end_date',
					'compare' => '>=',
					'value'   => $date,
					'type'    => 'numeric',
				),
			),
			array(
				'relation' => 'AND',
				array(
					'key'     => 'event_start_date',
					'compare' => '>=',
					'value'   => $date,
					'type'    => 'numeric',
				),
				array(
					'key'     => 'event_end_date',
					'compare' => '>=',
					'value'   => $date,
					'type'    => 'numeric',
				),
			),
		),
	);
	$posts_from_query = get_posts( $args );
	$posts_list       = [];

	foreach ( $posts_from_query as $single ) {
		$posts_list[ (string) $single->ID ] = $single->post_title;
	}
	unset( $posts_from_query );
	?>
	<div class="wrap">
	<h2>EVENTS settings</h2>
	<form method="post" action="options.php">
		<?php settings_fields( 'events_settings_group' ); ?>
		<table class="form-table">
			<tr valign="top">
				<th scope="row">Event on Homepage</th>
				<td>
					<?php
						$setting_val = get_option( 'event_settings_homepage' );
					if ( empty( $setting_val ) ) {
						$setting_val = '';
					}
					?>
					<select name="event_settings_homepage" size="10">
						<?php
						foreach ( $posts_list as $k => $post_single ) {
							$k        = (string) $k;
							$selected = '';
							if ( $k === $setting_val ) {
								$selected = ' selected="selected"'; }
							?>
							<option value="<?php echo $k; ?>" <?php echo $selected; ?>><?php echo $post_single; ?></option>
							<?php
						}
						?>
					</select>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">Default single Header image(id)</th>
				<td>
					<?php
						$setting_val = get_option( 'event_settings_default_header' );
					if ( empty( $setting_val ) ) {
						$setting_val = '';
					}
					?>
					<input name="event_settings_default_header" size="10" value="<?php echo $setting_val; ?>" />
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">Default Image #1(id)</th>
				<td>
					<?php
						$setting_val = get_option( 'event_settings_default_image_1' );
					if ( empty( $setting_val ) ) {
						$setting_val = '';
					}
					?>
					<input name="event_settings_default_image_1" size="10" value="<?php echo $setting_val; ?>" />
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">Default Image #2(id)</th>
				<td>
					<?php
						$setting_val = get_option( 'event_settings_default_image_2' );
					if ( empty( $setting_val ) ) {
						$setting_val = '';
					}
					?>
					<input name="event_settings_default_image_2" size="10" value="<?php echo $setting_val; ?>" />
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">Redirect to listign after(seconds)</th>
				<td>
					<?php
						$setting_val = get_option( 'event_settings_redirect' );
					if ( empty( $setting_val ) ) {
						$setting_val = '';
					}
					?>
					<input name="event_settings_redirect" size="10" value="<?php echo $setting_val; ?>" />
				</td>
			</tr>
		</table>
		<p class="submit">
		<input type="submit" class="button-primary" value="<?php _e( 'Save Changes' ); ?>" />
		</p>
	</form>
	</div>
	<?php
}
