<?php
/**
 * Add post types and categories.
 *
 * @return void
 */
function wpr_add_post_types() {
	// Event post type.
	$labels = array(
		'name'                  => _x( 'Events', 'Post Type General Name', 'wpr' ),
		'singular_name'         => _x( 'Event', 'Post Type Singular Name', 'wpr' ),
		'menu_name'             => __( 'Events', 'wpr' ),
		'name_admin_bar'        => __( 'Events', 'wpr' ),
		'archives'              => __( 'Events', 'wpr' ),
		'attributes'            => __( 'Event Attributes', 'wpr' ),
		'parent_item_colon'     => __( 'Parent Event:', 'wpr' ),
		'all_items'             => __( 'All Events', 'wpr' ),
		'add_new_item'          => __( 'Add New Event', 'wpr' ),
		'add_new'               => __( 'Add New', 'wpr' ),
		'new_item'              => __( 'New Event', 'wpr' ),
		'edit_item'             => __( 'Edit Event', 'wpr' ),
		'update_item'           => __( 'Update Event', 'wpr' ),
		'view_item'             => __( 'View Event', 'wpr' ),
		'view_items'            => __( 'View Events', 'wpr' ),
		'search_items'          => __( 'Search Event', 'wpr' ),
		'not_found'             => __( 'Not found', 'wpr' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'wpr' ),
		'featured_image'        => __( 'Featured Image', 'wpr' ),
		'set_featured_image'    => __( 'Set featured image', 'wpr' ),
		'remove_featured_image' => __( 'Remove featured image', 'wpr' ),
		'use_featured_image'    => __( 'Use as featured image', 'wpr' ),
		'insert_into_item'      => __( 'Insert into event', 'wpr' ),
		'uploaded_to_this_item' => __( 'Uploaded to this event', 'wpr' ),
		'items_list'            => __( 'Items list', 'wpr' ),
		'items_list_navigation' => __( 'Items list navigation', 'wpr' ),
		'filter_items_list'     => __( 'Filter items list', 'wpr' ),
	);
	$args   = array(
		'label'               => __( 'Event', 'wpr' ),
		'description'         => __( 'Events post type', 'wpr' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', 'custom-fields' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-calendar-alt',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'event', $args );

	// Press Release post type.
	$labels  = array(
		'name'                  => _x( 'Press releases', 'Post Type General Name', 'wpr' ),
		'singular_name'         => _x( 'Press release', 'Post Type Singular Name', 'wpr' ),
		'menu_name'             => __( 'Press releases', 'wpr' ),
		'name_admin_bar'        => __( 'Press releases', 'wpr' ),
		'archives'              => __( 'Press releases', 'wpr' ),
		'attributes'            => __( 'Press release Attributes', 'wpr' ),
		'parent_item_colon'     => __( 'Parent Press release:', 'wpr' ),
		'all_items'             => __( 'All Press releases', 'wpr' ),
		'add_new_item'          => __( 'Add New Press release', 'wpr' ),
		'add_new'               => __( 'Add New', 'wpr' ),
		'new_item'              => __( 'New Press release', 'wpr' ),
		'edit_item'             => __( 'Edit Press release', 'wpr' ),
		'update_item'           => __( 'Update Press release', 'wpr' ),
		'view_item'             => __( 'View Press release', 'wpr' ),
		'view_items'            => __( 'View Press release', 'wpr' ),
		'search_items'          => __( 'Search Press release', 'wpr' ),
		'not_found'             => __( 'Not found', 'wpr' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'wpr' ),
		'featured_image'        => __( 'Featured Image', 'wpr' ),
		'set_featured_image'    => __( 'Set featured image', 'wpr' ),
		'remove_featured_image' => __( 'Remove featured image', 'wpr' ),
		'use_featured_image'    => __( 'Use as featured image', 'wpr' ),
		'insert_into_item'      => __( 'Insert into Press release', 'wpr' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Press release', 'wpr' ),
		'items_list'            => __( 'Items list', 'wpr' ),
		'items_list_navigation' => __( 'Items list navigation', 'wpr' ),
		'filter_items_list'     => __( 'Filter items list', 'wpr' ),
	);
	$rewrite = array(
		'slug'       => 'press-release',
		'with_front' => true,
		'pages'      => true,
		'feeds'      => true,
	);
	$args    = array(
		'label'               => __( 'Press release', 'wpr' ),
		'description'         => __( 'Press release post type', 'wpr' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', 'custom-fields' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-media-spreadsheet',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => 'press-releases',
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'page',
	);
	register_post_type( 'press-releases', $args );

	// Press Release Tags.
	$labels  = array(
		'name'                       => _x( 'Tags', 'Taxonomy General Name', 'wpr' ),
		'singular_name'              => _x( 'Tag', 'Taxonomy Singular Name', 'wpr' ),
		'menu_name'                  => __( 'Tag', 'wpr' ),
		'all_items'                  => __( 'All Tags', 'wpr' ),
		'parent_item'                => __( 'Parent Tag', 'wpr' ),
		'parent_item_colon'          => __( 'Parent Tag:', 'wpr' ),
		'new_item_name'              => __( 'New Tag Name', 'wpr' ),
		'add_new_item'               => __( 'Add New Tag', 'wpr' ),
		'edit_item'                  => __( 'Edit Tag', 'wpr' ),
		'update_item'                => __( 'Update Tag', 'wpr' ),
		'view_item'                  => __( 'View Tag', 'wpr' ),
		'separate_items_with_commas' => __( 'Separate Tags with commas', 'wpr' ),
		'add_or_remove_items'        => __( 'Add or remove Tag', 'wpr' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'wpr' ),
		'popular_items'              => __( 'Popular Tags', 'wpr' ),
		'search_items'               => __( 'Search Items', 'wpr' ),
		'not_found'                  => __( 'Not Found', 'wpr' ),
		'no_terms'                   => __( 'No Tags', 'wpr' ),
		'items_list'                 => __( 'Tags list', 'wpr' ),
		'items_list_navigation'      => __( 'Tags list navigation', 'wpr' ),
	);
	$rewrite = array(
		'slug'         => 'press-releases-tag',
		'with_front'   => true,
		'hierarchical' => false,
	);
	$args    = array(
		'rewrite'           => $rewrite,
		'labels'            => $labels,
		'hierarchical'      => false,
		'public'            => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud'     => true,
	);
	register_taxonomy( 'press-releases-tag-cat', array( 'press-releases' ), $args );
}
add_action( 'init', 'wpr_add_post_types' );

/**
 * Flush Rewrite on theme changes.
 *
 * @return void
 */
function wpr_rewrite_flush() {
	wpr_add_post_types();
	flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'wpr_rewrite_flush' );

// Add Events Admin Columns.
function wpr_events_admin_columns( $columns ) {
	$column_meta = array( 'status' => 'Status<br />(End date)' );
	$columns     = array_slice( $columns, 0, 2, true ) + $column_meta + array_slice( $columns, 2, null, true );
	return $columns;
}

function wpr_events_admin_sortable_columns( $columns ) {
	$columns['status'] = 'status';
	return $columns;
}

function wpr_add_events_column_orderby( $vars ) {
	if ( isset( $vars['orderby'] ) && 'status' == $vars['orderby'] ) {
		$vars = array_merge(
			$vars,
			array(
				'meta_key' => 'event_end_date',
				'orderby'  => 'meta_value_num',
			)
		);
	}

	return $vars;
}

function wpr_add_events_custom_columns( $column ) {
	global $post;
	switch ( $column ) {
		case 'status':
			$end_date = get_post_meta( $post->ID, 'event_end_date', true );
			if ( empty( $end_date ) ) {
				echo '<div> - </div>';
			} else {
				$color_add = 'green';
				$today     = (int) date( 'Ymd' );
				if ( $today > $end_date ) {
					$color_add = 'red';
				}

				$end_date = new DateTime( $end_date );
				echo '<div style="color: ' . esc_html( $color_add ) . ';">' . esc_html( $end_date->format( 'F' ) ) . ' ' . esc_html( $end_date->format( 'd' ) ) . ' ' . esc_html( $end_date->format( 'Y' ) ) . '</div>';
			}
			break;
	}
}

add_filter( 'manage_edit-event_columns', 'wpr_events_admin_columns' );
add_filter( 'manage_edit-event_sortable_columns', 'wpr_events_admin_sortable_columns' );
add_filter( 'request', 'wpr_add_events_column_orderby' );
add_action( 'manage_posts_custom_column', 'wpr_add_events_custom_columns' );
