jQuery( document ).ready(function($) {
    if( $('.et_pb_search.wpr_search_form').length ){
        $('.et_pb_searchsubmit').val('');
    }

    if( $('#press_release_archive').length ){
        var delay_time  = 2000;
        $('<div id="top_navigation_div"></div>').insertBefore('#press_release_archive');
        $('#top_navigation_div').html($('#press_release_archive .pagination').clone());

        $('body').on('click', '#top_navigation_div a', function(e){
            e.preventDefault();
            if( $(this).hasClass('disabled') ){

            }
            else{
                $('#top_navigation_div a').addClass('disabled');
                var page_id = $(this).attr('data-page');
                $('#press_release_archive .pagination a[data-page='+page_id+']').trigger('click');
                setTimeout(function() {
                    $('#top_navigation_div').html($('#press_release_archive .pagination').clone());
                }, delay_time);
            }
        });

        $("body").on("click","#press_release_archive .pagination a", function(){
            $('#top_navigation_div a').addClass('disabled');
            setTimeout(function() {
                $('#top_navigation_div').html($('#press_release_archive .pagination').clone());
            }, delay_time);
        });
    }
});
