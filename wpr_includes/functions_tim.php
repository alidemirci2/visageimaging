<?php
/* TIM ADDED */
function wpr_assets_add() {
	wp_enqueue_style( 'wpr-tim-style', trailingslashit( get_stylesheet_directory_uri() ) . 'wpr_includes/wpr.css', array(), date( 'Y-m-d-h-i-s' ) );
	wp_enqueue_script( 'wpr-tim-script', trailingslashit( get_stylesheet_directory_uri() ) . 'wpr_includes/wpr.js', array(), date( 'Y-m-d-h-i-s' ), true );
}

add_action( 'wp_enqueue_scripts', 'wpr_assets_add' );

add_action( 'pre_get_posts', 'wpr_add_search_post_type', 99 );
function wpr_add_search_post_type( $query ) {
	if ( is_admin() || ! is_a( $query, 'WP_Query' ) || ! $query->is_search ) {
		return;
	}

	$query->set( 'post_type', array( 'press-releases' ) );
	return $query;
}

function wpr_body_classes( $classes ) {
	global $post;
	$body_class_add = get_field( 'body_class_add', $post->ID );

	if ( ! empty( $body_class_add ) ) {
		$classes[] = $body_class_add;
	}

	if ( is_singular( 'event' ) || is_singular( 'press-releases' ) || is_archive( 'press-releases-tag-cat' ) || is_search() ) {
		$classes[] = 'wpr_background_hex';
	}

	return $classes;
}
add_filter( 'body_class', 'wpr_body_classes' );

function wpr_show_period( $post_id, $start_date = 'empty', $end_date = 'empty' ) {
	$html = '';
	if ( 'empty' === $start_date ) {
		$start_date = get_field( 'event_start_date', $post_id );
	}
	if ( 'empty' === $end_date ) {
		$end_date = get_field( 'event_end_date', $post_id );
	}

	if ( ! empty( $start_date ) ) {
		$start_date = new DateTime( $start_date );
	}
	if ( ! empty( $end_date ) ) {
		$end_date = new DateTime( $end_date );
	}

	if ( ! empty( $start_date ) ) {
		if ( empty( $end_date ) || $start_date == $end_date ) {
			$html .= $start_date->format( 'F d Y' );
		} else {
			$st   = array();
			$st[] = $start_date->format( 'd' );
			$st[] = $start_date->format( 'm' );
			$st[] = $start_date->format( 'Y' );

			$end   = array();
			$end[] = $end_date->format( 'd' );
			$end[] = $end_date->format( 'm' );
			$end[] = $end_date->format( 'Y' );

			if ( $st[2] === $end[2] ) {
				if ( $st[1] === $end[1] ) {
					if ( $st[0] !== $end[0] ) {
						$html .= $start_date->format( 'F d' ) . '-' . $end_date->format( 'd' ) . ', ' . $start_date->format( 'Y' );
					}
				} else {
					$html .= $start_date->format( 'F d' ) . ' - ' . $end_date->format( 'F d' ) . ', ' . $start_date->format( 'Y' );
				}
			} else {
				$html .= $start_date->format( 'F d Y' ) . ' - ' . $end_date->format( 'F d Y' );
			}
		}
	}

	return $html;
}

function wpr_taxonomy_release_cat( $links ) {
	if ( ! empty( $links ) ) {
		array_unshift( $links, '<div class="press_release_categories">' );
		array_push( $links, '</div>' );
	}
	return $links;
}
add_filter( 'term_links-press-releases-tag-cat', 'wpr_taxonomy_release_cat', 10, 1 );

/**
 * Get latest blog post data from Hubspot
 *
 * @return void
 */
$wpr_post_transient_name = 'blog_latest_article_data';
function get_hubspot_data_latest_post() {
	global $wpr_post_transient_name;
	$wpr_post_transient_expiration = 60 * 60 * 12;
	$wpr_post_url                  = 'https://api.hubapi.com/content/api/v2/blog-posts?hapikey=75d19120-b4c9-41ad-ad3d-931c63e43a9b'; // Keys: 75d19120-b4c9-41ad-ad3d-931c63e43a9b   ||  demo
	$request                       = wp_remote_get( $wpr_post_url . '&limit=1&order_by=-publish_date' );
	if ( is_wp_error( $request ) ) {
		return;
	}

	$body = wp_remote_retrieve_body( $request );
	$body = json_decode( $body );
	if ( ! empty( $body ) ) {
		$data = array();

		$data['image'] = $body->objects[0]->featured_image;
		$data['title'] = $body->objects[0]->html_title;
		$data['body']  = $body->objects[0]->post_summary;
		$data['link']  = 'https://blog.visageimaging.com/' . $body->objects[0]->slug;

		set_transient( $wpr_post_transient_name, json_encode( $data, $wpr_post_transient_expiration ) );
		unset( $date );
		unset( $body );
	} else {
		return;
	}
	unset( $request );
}

if ( false === get_transient( $wpr_post_transient_name ) ) {
	get_hubspot_data_latest_post();
}

require_once 'settings.php';
require_once 'post_types.php';
require_once 'shortcodes.php';
