<?php

get_header();

$show_default_title = get_post_meta( get_the_ID(), '_et_pb_show_title', true );

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );

$start_date = get_field( 'event_start_date', $post_id );
$end_date   = get_field( 'event_end_date', $post_id );
$now        = date( 'Ymd' );

?>

<div id="main-content">
	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">
				<?php
				while ( have_posts() ) :
					the_post();
					?>
					<?php
					/**
					 * Fires before the title and post meta on single posts.
					 *
					 * @since 3.18.8
					 */
					do_action( 'et_before_post' );
					?>
					<article id="post-<?php the_ID(); ?>" <?php post_class( 'et_pb_post' ); ?>>
						<?php
						do_action( 'et_before_content' );
						$header_image = get_field( 'event_header_image', get_the_id() );
						if ( empty( $header_image ) || is_null( $header_image ) ) {
							$temp         = get_option( 'event_settings_default_header' );
							$header_image = wp_get_attachment_image_src( $temp, 'full' );
							$header_image = $header_image[0];
						} else {
							$header_image = $header_image['url'];
						}
						$header_image_html = '';
						if ( ! empty( $header_image ) ) {
							$header_image_html = 'background-image: url(' . trailingslashit( get_stylesheet_directory_uri() ) . 'img/event_header_gradient.png), linear-gradient(214deg, rgba(125,0,250,0.6) 0%, rgba(198,114,250,0.6) 100%), url(' . $header_image . ');';
						}
						?>
						<div id="event_header" style="<?php echo $header_image_html; ?>">
							<h1><?php echo get_the_title(); ?></h1>
							<div class="period"><?php echo wpr_show_period( get_the_id(), $start_date, $end_date ); ?></div>
							<?php
							$location = get_field( 'event_location', get_the_id() );
							if ( ! empty( $location ) ) {
								echo '<div class="location">' . $location . '</div>';
							}
							?>
						</div>
						<?php
						$event_ended = $now > $end_date;
						$class_add   = '';
						if ( $event_ended ) {
							$class_add = 'event_ended';
						}

						?>
						<div class="entry-content <?php echo $class_add; ?>">
							<div class="col_1">
								<?php
								$image = get_field( 'event_content_logo_meeting', get_the_id() );
								if ( ! empty( $image ) ) {
									?>
										<div class="logo_meeting_image"><img src="<?php echo $image['url']; ?>" alt="<?php echo get_the_title(); ?>" /></div>
									<?php
								}
								?>
								<?php if ( ! $event_ended ) { ?>
									<div class="content_div">
										<?php
											do_action( 'et_before_content' );

											the_content();
										?>
									</div>
								<?php } else { ?>
									<?php
									$expired_html = get_field( 'event_expired_text', get_the_id() );
									if ( ! empty( $location ) ) {
										echo '<div class="expired_html">' . $expired_html . '</div>';
									}
									?>
									<div class="expired_button_wrapper">
										<a href="<?php echo get_field( 'event_expired_btn_link', get_the_id() ); ?>" class="button"><?php echo get_field( 'event_expired_btn_text', get_the_id() ); ?></a>
									</div>
									<div class="event_images_div">
									<?php
										$image = get_field( 'event_content_image_1', get_the_id() );
									if ( empty( $image ) || is_null( $image ) ) {
										$temp  = get_option( 'event_settings_default_image_1' );
										$image = wp_get_attachment_image_src( $temp, 'full' );
										$image = $image[0];
									} else {
										$image = $image['url'];
									}

									if ( ! empty( $image ) ) {
										?>
												<div class="event_image"><img src="<?php echo $image; ?>" alt="<?php echo get_the_title(); ?>" /></div>
											<?php
									}

											$image = get_field( 'event_content_image_2', get_the_id() );
									if ( empty( $image ) || is_null( $image ) ) {
										$temp  = get_option( 'event_settings_default_image_2' );
										$image = wp_get_attachment_image_src( $temp, 'full' );
										$image = $image[0];
									} else {
										$image = $image['url'];
									}
									if ( ! empty( $image ) ) {
										?>
												<div class="event_image"><img src="<?php echo $image; ?>" alt="<?php echo get_the_title(); ?>" /></div>
											<?php
									}
									?>
									</div>
									<script type="text/javascript">
										window.setTimeout(function(){
											window.location.href = "<?php echo get_home_url(); ?>/events";
										}, 
										<?php
											$setting_val = get_option( 'event_settings_redirect' );
										if ( empty( $setting_val ) ) {
											$setting_val = '5';
										}
											echo (int) $setting_val * 1000;
										?>
										);
									</script>
									<?php
								}
								?>
							</div>
							<?php if ( ! $event_ended ) : ?>
							<div class="col_2">
								<h3>Visage Imaging Priority Demo</h3>
								<div class="hubspot_form">
									<?php echo do_shortcode( '[hubspot type=form portal=2229016 id=9315f9b2-797d-4e93-87a8-f2933b59097d]' ); ?>
								</div>
							</div>
							<?php endif; ?>
						</div> <!-- .entry-content -->
						<div class="et_post_meta_wrapper">
							<?php

							/**
							 * Fires after the post content on single posts.
							 *
							 * @since 3.18.8
							 */
							do_action( 'et_after_post' );
							?>
						</div> <!-- .et_post_meta_wrapper -->
					</article> <!-- .et_pb_post -->

				<?php endwhile; ?>
			</div> <!-- #left-area -->
		</div> <!-- #content-area -->
	</div> <!-- .container -->
</div> <!-- #main-content -->

<?php

get_footer();
