<?php

get_header();

$show_default_title = get_post_meta( get_the_ID(), '_et_pb_show_title', true );

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );
?>
<?php echo do_shortcode( '[load_divi_layout id="5337"]' ); ?>
<div id="main-content">
	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">
				<div class="et_pb_section et_pb_with_background et_section_regular" id="breadcrumbs_section">
					<?php echo do_shortcode( '[load_divi_layout id="5349"]' ); ?>
				</div>
				<?php echo the_archive_title( '<h1 class="single_title">', '</h1>' ); ?>
				<div id="top_navigation_div">
				<?php
					get_template_part( 'includes/navigation', 'index' );
				?>
				</div>
				<div id="taxonomy_content_div" class="wpr_realeses_listing">
					<?php if ( have_posts() ) : ?>
						<?php
						while ( have_posts() ) :
							the_post();
							?>
							<article id="post-<?php echo get_the_ID(); ?>" class="press-releases type-press-releases">
								<h2 class="entry-title"><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></h2>
								<p class="post-meta">
									<span class="published"><?php echo get_the_date( 'M d, Y' ); ?></span>
								</p>
								<p></p>
								<div class="post-content">
									<div class="post-content-inner">
										<?php the_excerpt(); ?>
									</div>
									<a href="<?php echo get_permalink(); ?>" class="more-link"><?php echo __( 'Full Article', 'wpr' ); ?></a>
								</div>
							</article>
						<?php endwhile; ?>
						<?php
						get_template_part( 'includes/navigation', 'index' );
					else :
						echo '<p class="no_posts_found">' . esc_html__( 'No press release found', 'wpr' ) . '</p>';
					endif;
					?>
				</div> <!-- .entry-content -->
			</div> <!-- #left-area -->
		</div> <!-- #content-area -->
	</div> <!-- .container --> 
	<div class="section" id="under_text_elements">
		<?php echo do_shortcode( '[load_divi_layout id="5419"]' ); ?>
		<?php echo do_shortcode( '[load_divi_layout id="4555"]' ); ?>
		<?php echo do_shortcode( '[load_divi_layout id="3023"]' ); ?>
	</div>
</div> <!-- #main-content -->

<?php

get_footer();
