<?php
	global $paged, $wp_query;

	$count = $wp_query->max_num_pages;
if ( 1 < $count ) {
	$current = (int) $paged;
	if ( 0 === $current ) {
		$current = 1;
	}

	?>
<div class="pagination clearfix">
	<div class="arrow left_arrow"><?php previous_posts_link( '<svg version="1.0" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" width="13" aria-hidden="true"><g id="layer1"><path d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z"></path></g></svg>' ); ?></div>
	<?php
	$dots_start = '';
	$dots_end   = '';
	$start      = 1;
	$end        = $count;
	$show       = 5;
	if ( $count > $show ) {
		if ( $current > 3 ) {
			$start      = $current - 2;
			$dots_start = '<div class="page">...</div>';
		} else {
			$start = 1;
		}
		if ( $current < ( $count - 2 ) ) {
			$end      = $current + 2;
			$dots_end = '<div class="page">...</div>';
		} else {
			$end = $count;
		}
	}

	echo $dots_start;
	for ( $i = $start; $i <= $end; $i++ ) {
		$link_page = '/press-releases/page/' . $i . '/?et_blog';
		if ( 1 === $i ) {
			$link_page = '/press-releases/';
		}
		$classes = [];
		if ( $i === $current ) {
			$classes[] = 'selected';
		}
		?>
		<div class="page <?php echo implode( ' ', $classes ); ?>"><a href="<?php echo esc_html__( $link_page ); ?>" data-page="<?php echo esc_html__( $i ); ?>"><?php echo esc_html__( $i ); ?></a></div>
		<?php
	}
	echo $dots_end;
	?>
	<div class="arrow right_arrow"><?php next_posts_link( '<svg version="1.0" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" width="13" aria-hidden="true"><g id="layer1"><path d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"></path></g></svg>' ); ?></div>
</div>
	<?php
}
?>
